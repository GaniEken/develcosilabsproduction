# SiLabs BLE module configuration for production testing #
* The application software is based on the SiLabs NCP UART/Serial network co-processor implementation. It implements addtional functions for production testing.   
* BGAPI serial protocol
* UART baud rate: 115200
* HW flow control disabled
* Data bits: 8
* Stop bits: 1
* No parity
* RX pin: PA1
* TX pin: PA0

# Command #
| Byte | Type | Name | Description |
| ------ | ------ | ------ | ------ |
| 0 | 0x20 | hilen | Message type: Command |
| 1 | 0x09 | lolen | Payload length |
| 2 | 0xff | class | Message class: User messaging |A
| 3 | 0x00 | method | Message ID |
| 4 | uint8 | length | Array length: 8 |
| 5 | uint8 | timeout | Timeout in seconds: >=1 |
| 6 | uint8 | sample num | Number of samples: >= 1|
| 7-12 | bd_addr | address | Address of the testharness|

# Response #
| Byte | Type | Name | Description |
| ------ | ------ | ------ | ------ |
| 0 | 0x20 | hilen | Message type: Response |
| 1 | uint8 | lolen | Payload length<br>3: in case of an error<br>4: when successful |
| 2 | 0xff | class | Message class: User messaging |
| 3 | 0x00 | method | Message ID |
| 4-5 | uint16 | result | Result code:<br>* 0x0000: success<br>* 0x0180: invalid parameter when e.g. timeout or sample num is 0<br> * 0x0185: timeout|
| 6 | uint8 | length | 0: in case of an error<br>1: when successful   |
| 7 | int8 | RSSI | Median (see number of samples) of RSSI |



#  SDK configuration  #
* Gecko SDK Suite v2.5.5
* Bluetooth Stack v2.11.5.0
* Toolchain GNU ARM v7.2.1
* Gecko Bootloader v1.8.4

# Installation #
1. Download and install Silabs Simplicity Studio v4
2. Clone this repository
3. Import the repository into Silabs Simplicity Studio

# Creating combined image file which contains bootloader bluetooth stack and NCP application
1. Copy the bootloader "develcoUartBootloader-combined.s37" and application (includes the bluetooth stack) develcoIAR.s37 in the same folder
2. The bootloader file can be found in the "GNU ARM v7.2.1 - Default" folder
3. Open command promt and change to the folder which contains the bootloader and the application files
4. Run the command "C:\SiliconLabs\SimplicityStudio\v4\developer\adapter_packs\commander\commander.exe convert develcoUartBootloader-combined.s37 develcoTesting.s37 -o develco_silabs_bootloader_plus_app.s37"
5. The combined image file will be saved in "develco_silabs_bootloader_plus_app_testing.s37"

# Programming the combined image file
| Step | Description | 
| ------ | ------ |
| 1 | Open Simplicity Commander. Open Windows->Preferences  |
| 2 | Select Simplicity Studio-> Adapter Packs|
| 3 | Select Simplicity Commander [build-in] -> commander.exe for win32.x86_64 in Adapter Packs window and click the "Bare run" button |
| 4 | Make sure that the debugger is connected to the BLE module and the BLE module is powered up |
| 5 | Select J-Link Device and click the "Connect" button next to "Adapter" |
| 6 | Click "Connect" button next to Target |
| 7 | Switch to "Kit" tab and make sure that Debut mode is set to Out |
| 8 | Switch to Flash tab |
| 9 | Select the file "develco_silabs_bootloader_plus_app_testing.s37" and click the Flash button

