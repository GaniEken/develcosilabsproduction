/***************************************************************************//**
 * @file
 * @brief Silabs Network Co-Processor (NCP) library
 * This library allows customers create applications work in NCP mode.
 *******************************************************************************
 * # License
 * <b>Copyright 2018 Silicon Laboratories Inc. www.silabs.com</b>
 *******************************************************************************
 *
 * The licensor of this software is Silicon Laboratories Inc. Your use of this
 * software is governed by the terms of Silicon Labs Master Software License
 * Agreement (MSLA) available at
 * www.silabs.com/about-us/legal/master-software-license-agreement. This
 * software is distributed to you in Source Code format and is governed by the
 * sections of the MSLA applicable to Source Code.
 *
 ******************************************************************************/

#include "ncp_gecko.h"
#include "ncp.h"

static uint8_t s_testTimeout;
static uint8_t s_rssiMedianLength;
static bd_addr s_macAddress;
static int8_t s_rssiArray[256];
static uint16_t s_rssiIndex;

/**
 * Default implementation to user command handling.
 *
 * @param payload the data payload
 */
void handle_user_command(uint8_t* data)
{
    /* Makes sure that scanning is not running */
    gecko_cmd_le_gap_end_procedure();

    if ((8 == data[0]) &&
        (0 != data[1]) &&
        (0 != data[2]))
    {

        s_testTimeout = data[1];
        s_rssiMedianLength = data[2];

        s_macAddress.addr[5] = data[3];
        s_macAddress.addr[4] = data[4];
        s_macAddress.addr[3] = data[5];
        s_macAddress.addr[2] = data[6];
        s_macAddress.addr[1] = data[7];
        s_macAddress.addr[0] = data[8];

        /* Resets the index */
        s_rssiIndex = 0;

        /* Sets scan paramter */
        gecko_cmd_le_gap_set_discovery_timing(le_gap_phy_1m, 16, 16);
        /*
         * Sets discovery type
         * LE 1M PHY and passive scanning
         */
        gecko_cmd_le_gap_set_discovery_type(le_gap_phy_1m, 0);
        /* Starts discover mode
         *
         * 0  le_gap_discover_limited       Discover only limited discoverable devices
         * 1  le_gap_discover_generic       Discover limited and generic discoverable devices
         * 2  le_gap_discover_observation   Discover all devices
        */
        gecko_cmd_le_gap_start_discovery(le_gap_phy_1m, le_gap_discover_generic);

        /*
         * Starts timeout
         * interval: s_testTimeout seconds
         * Handle no: 0
         * Not repeating timer
        */
        gecko_cmd_hardware_set_soft_timer(32768 * s_testTimeout, 0, 1);
    }
    else
    {
        gecko_send_rsp_user_message_to_target(bg_err_invalid_param, 0, NULL);
        ncp_transmit_enqueue((struct gecko_cmd_packet *)gecko_rsp_msg_buf);
    }
}


void test_scan_response(struct gecko_msg_le_gap_scan_response_evt_t *evt)
{
    if ((evt->address.addr[0] == s_macAddress.addr[0]) &&
        (evt->address.addr[1] == s_macAddress.addr[1]) &&
        (evt->address.addr[2] == s_macAddress.addr[2]) &&
        (evt->address.addr[3] == s_macAddress.addr[3]) &&
        (evt->address.addr[4] == s_macAddress.addr[4]) &&
        (evt->address.addr[5] == s_macAddress.addr[5]))
    {
        /* Saves the RSSI value in the array */
        s_rssiArray[s_rssiIndex++] = evt->rssi;

        /* Checks if required number of rssi values have been collected */
        if (s_rssiIndex >= s_rssiMedianLength)
        {
            int8_t median;

            /* Stops the timer */
            gecko_cmd_hardware_set_soft_timer(0, 0, 1);

            /* Stops scanning */
            gecko_cmd_le_gap_end_procedure();

            if (1 == s_rssiMedianLength)
            {
                median = s_rssiArray[0];
            }
            else
            {
                uint16_t i, j;
                uint8_t temp;

                /* Sorts array in order to calculate median */
                for (i = 0; i < s_rssiIndex; i++)
                {
                    for (j = 0; j < (s_rssiIndex - 1); j++)
                    {
                        if (s_rssiArray[j] > s_rssiArray[j + 1])
                        {
                            temp                = s_rssiArray[j];
                            s_rssiArray[j]      = s_rssiArray[j + 1];
                            s_rssiArray[j + 1]  = temp;
                        }
                    }
                }

                /* Picks the value at the center of the sorted array */
                if (0 == s_rssiIndex % 2)
                {
                    median = ((int16_t) s_rssiArray[(s_rssiIndex - 1) / 2] + (int16_t) s_rssiArray[s_rssiIndex / 2]) / 2;
                }
                else
                {
                    median = s_rssiArray[s_rssiIndex / 2];
                }
            }

            /* Sends response to the host */
            gecko_send_rsp_user_message_to_target(bg_err_success, 1, (uint8_t *) &median);
            ncp_transmit_enqueue((struct gecko_cmd_packet *)gecko_rsp_msg_buf);
        }
    }

}
